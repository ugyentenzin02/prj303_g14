# PRJ303 (Visualization and forecasting of covid 19) Year 3 project

This system will enable the users to visualize the covid-19 situation globally and locally in terms of confirmed cases, death cases, and vacinnation coverage around the world. It will also be used to forecast future possible positive cases and death cases based on prior observations.
 


## Built using:
HTML/CSS/JS,Bootstrap,django,python,pandas,scikit-learn,Matplotlib. 






## promotional video link:
https://drive.google.com/file/d/1kYloJdJUQVvvNfqzeje5uvAhYn_MioDF/view?usp=sharing







## Website Link : 
http://covid-forcasting.herokuapp.com/




## Dataset Link:
cases: https://covid19.who.int/WHO-COVID-19-global-table-data.csv
vacinnation : https://covid19.who.int/who-data/vaccination-metadata.csv
